# coding: utf-8
import snowboydecoder
import sys
import signal
import subprocess
import recognition
import remo
import dialogflow_api
import os

TOP_DIR = os.path.dirname(os.path.abspath(__file__))

interrupted = False

def signal_handler(signal, frame):
    global interrupted
    interrupted = True

def change_interrupted():
    global interrupted
    interrupted = True

def interrupt_callback():
    return interrupted

def proceed():
	subprocess.call("aplay -D plughw:1,0  " + TOP_DIR + "/../resources/start.wav", shell=True)
	recognition.record()
	subprocess.call("aplay -D plughw:1,0 " + TOP_DIR + "/../resources/end.wav", shell=True)
	text = recognition.recognition()
	appliance, operation = dialogflow_api.access(text)
	remo.access(appliance, operation)

# main loop
while 1:
	detector = snowboydecoder.HotwordDetector(TOP_DIR + "/../resources/jony.pmdl", sensitivity=0.5)
	print('「Hey Jony」って話しかけて！')
	detector.start(detected_callback=change_interrupted,
               	interrupt_check=interrupt_callback,
               	sleep_time=0.03)
	detector.terminate()
	proceed()
	interrupted=False

