## coding: UTF-8
import speech_recognition
import pyaudio
import wave
import os

TOP_DIR = os.path.dirname(os.path.abspath(__file__))

RECORD_SECONDS = 5         # 録音する時間
FILENAME = TOP_DIR + '/../resources/record.wav'     # 保存するファイル名
iDeviceIndex = 2            # 録音デバイスの番号
FORMAT = pyaudio.paInt16    # 音声フォーマット
CHANNELS = 1                # チャンネル数（モノラル）
RATE = 16000                # サンプリングのレート
CHUNK = 1024 * 2               # データ点数
KEY = os.environ['RECOGNIZE_GOOGLE_KEY']  # GoogleSearchRecognition APIキー

def record():
    audio = pyaudio.PyAudio()
    stream = audio.open(format=FORMAT, channels=CHANNELS,
                        rate=RATE, input=True,
                        input_device_index=iDeviceIndex,
                        frames_per_buffer=CHUNK)
    print("recording...")       # 録音開始
    frames = []
    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)
    print("finish recording")   # 録音終了

    stream.stop_stream()
    stream.close()
    audio.terminate()

    waveFile = wave.open(FILENAME, 'wb')
    waveFile.setnchannels(CHANNELS)
    waveFile.setsampwidth(audio.get_sample_size(FORMAT))
    waveFile.setframerate(RATE)
    waveFile.writeframes(b''.join(frames))
    waveFile.close()

def recognition():
    r = speech_recognition.Recognizer()
    with speech_recognition.AudioFile(FILENAME) as src:
        audio = r.record(src)
    result = r.recognize_google(audio, key=KEY, language='ja-JP')
    print(result)
    return result
