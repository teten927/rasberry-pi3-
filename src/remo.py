# coding: utf-8
import os
import requests

TOKEN = os.environ['TOKEN']		# Nature Remo アクセストークン
LIGHT_ID = os.environ['LIGHT']	# 照明のRemo ID
TV_ID = os.environ['TV']		# TVのRemo ID
AIRCON_ID = os.environ['AIRCON']# エアコンのRemo ID

# Nature RemoへのAPIアクセス用関数
def access(appliance, operation):
	url = "https://api.nature.global/1/appliances/{}/{}".format(LIGHT_ID, appliance)
	params = {
	'button': operation
	}

	headers = {
	'Authorization': 'Bearer {}'.format(TOKEN),
	'accept': 'application/json',
	'Content-Type': 'application/x-www-form-urlencoded'
	}
	response = requests.post(url, data=params, headers=headers)

