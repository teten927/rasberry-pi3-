# coding: utf-8
import os
import dialogflow
from google.api_core.exceptions import InvalidArgument

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'private_key.json'	# DialogFlowアクセス用クレデンシャル

DIALOGFLOW_PROJECT_ID = os.environ["PROJECT_ID"]	# DialogFlowプロジェクトID
DIALOGFLOW_LANGUAGE_CODE = "ja"
SESSION_ID = "me"

# Google DialogFlowAPIアクセス用関数
def access(text):
	session_client = dialogflow.SessionsClient()
	session = session_client.session_path(DIALOGFLOW_PROJECT_ID, SESSION_ID)
	text_input = dialogflow.types.TextInput(text=text, language_code=DIALOGFLOW_LANGUAGE_CODE)
	query_input = dialogflow.types.QueryInput(text=text_input)

	try:
		response = session_client.detect_intent(session=session, query_input=query_input)
	except InvalidArgument:
   		raise

	appliance = str(response.query_result.fulfillment_messages[1].payload.fields[u'appliance'].string_value)
	operation = str(response.query_result.fulfillment_messages[1].payload.fields[u'operation'].string_value)
	return appliance, operation
